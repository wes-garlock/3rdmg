#Created by Wesley Garlock Version 1.0 Published June 8th 2016
from django.contrib import admin
from .models import BackendBuilding, BackendVentureCapital
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User


# Register your models here.

class BackendBuildingAdmin(admin.ModelAdmin):
    list_display = ["__unicode__",  "propname", "category", "country", "city", "state", "zipcode", "updated" ]
    list_display_links = ["__unicode__", "propname"]
    list_filter = ["country", "city", "state", "zipcode", "updated"]
    search_fields = ["__unicode__",  "propname", "country", "city", "state", "zipcode"]

    class Meta:
        model = BackendBuilding

admin.site.register(BackendBuilding, BackendBuildingAdmin)

class BackendVentureCapitalAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "vname", 'updated'  ]
    list_display_links = ["__unicode__",]
    list_filter = []
    search_fields = []

    class Meta:
        model = BackendVentureCapital

admin.site.register(BackendVentureCapital, BackendVentureCapitalAdmin)


from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User

from .models import Personal_Info

# Define an inline admin descriptor for Employee model
# which acts a bit like a singleton
class Personal_InfoInline(admin.StackedInline):
    model = Personal_Info
    can_delete = False
    verbose_name_plural = 'Personal Info'

# Define a new User admin
class UserAdmin(BaseUserAdmin):
    inlines = (Personal_InfoInline, )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
