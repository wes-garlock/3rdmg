#Created by Wesley Garlock Version 1.0 Published June 8th 2016
from django import forms
from django.contrib.auth.models import User
from .models import Personal_Info

# forms.py
class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']

class PersonalForm(forms.ModelForm):
    class Meta:
        model = Personal_Info
        fields = ['Cell_Phone', 'Home_Phone', 'Street', 'City', 'State', 'Zipcode', 'Country']
