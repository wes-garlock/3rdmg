#Created by Wesley Garlock Version 1.0 Published June 8th 2016
import boto, boto3, sys, os, csv, json
from pdfrw import PdfReader, PdfWriter, PageMerge
from reportlab.pdfgen import canvas
from datetime import datetime
from reportlab.lib.colors import Color
from reportlab.lib.units import inch
from django.conf import settings
from .models import BackendBuilding

def watermarktool(a):
    # Creates Watermark
    def watermark(c):
        name = 'Example Example - '
        email = 'example@gmail.com - '
        date = datetime.now()
        date  = str(date)
        mark = name + email + date
        watercolor = Color(255, 0 , 0, alpha = 0.10 )
        c.translate(inch,inch)
        c.setFont("Helvetica", 14)
        c.rotate(57)
        c.setFillColor(watercolor)
        c.drawString(3*inch, 0*inch, mark)

    # Adds Watermark
    def watermarker(a):
        argv = [a, 'watermark.pdf']
        underneath = '-u' in argv
        if underneath:
            del argv[argv.index('-u')]
        inpfn, wmarkfn = argv
        outfn = 'watermark.' + os.path.basename(inpfn)
        wmark = PageMerge().add(PdfReader(wmarkfn).pages[0])[0]
        trailer = PdfReader(inpfn)
        for page in trailer.pages:
            PageMerge(page).add(wmark, prepend=underneath).render()
        PdfWriter().write(outfn, trailer)

    #######################################################################

    # Create the watermark
    c = canvas.Canvas("watermark.pdf")
    watermark(c)
    c.showPage()
    c.save()

    # Add the watermark
    watermarker(a)

    #######################################################################
    #######################################################################
    #######################################################################

def csvconv(r):
    if not settings.DEBUG:
        f         = open(r)
    else:
        f         = open(r.findata.path)

    csvReader = csv.reader(f)
    a         = []
    b         = []
    i         = 0

    for row in csvReader:
        a.append(row)
    for i in range(0,len(a)):
        for j in range(0, len(a[i])):
            a[i][j] = str(a[i][j])
            a[i][j] = a[i][j].decode('utf-8')

    for j in range(1, len(a[0])):
        a[0][j] = int(a[0][j])
        b.append(a[0][j])

    for i in range(1,len(a)):
        for j in range(1, len(a[i])):
            a[i][j] = float(a[i][j])

    maxi = max(b)
    mini = min(b)
    length = len(b)

    a = zip(*a)
    a = json.dumps(a)

    context = {
        "a"      : a,
        "maxi"   : maxi,
        "mini"   : mini,
        "length" : length,
    }
    return context




def csvpull(instance):

    AWS_ACCESS_KEY_ID       = 'AKIAJKAOUNIOUFMS527Q'
    AWS_SECRET_ACCESS_KEY   = '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'
    bucket_name = 'tdmilgrp'
    conn = boto.connect_s3(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY)
    bucket = conn.get_bucket(bucket_name)
    bucket_list = bucket.list()
    r = instance.findata.url
    r = r[34:]
    for i in bucket_list:
        if i.key == r:
            r = r[9:]
            LOCAL_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
            LOCAL_PATH = os.path.join(LOCAL_PATH, 'static', 'media', 'csv')
            if not os.path.exists(LOCAL_PATH + r):
                i.get_contents_to_filename(LOCAL_PATH + r )
                r = LOCAL_PATH + r
            else:
                os.remove(LOCAL_PATH + r)
                i.get_contents_to_filename(LOCAL_PATH + r )
                r = LOCAL_PATH + r
            return r


def csvapp (instance):
    if not settings.DEBUG:
        r = csvpull(instance)
        context = csvconv(r)
        return context
    else:
        context = csvconv(instance)
        return context
