#Created by Wesley Garlock Version 1.0 Published June 8th 2016

from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.db.models.fields import EmailField, IntegerField, CharField, BooleanField
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User

class BackendBuilding(models.Model):
    propid   = models.CharField(max_length=6)
    closed   = BooleanField(default=False)
    slug     = models.SlugField(unique=True)
    image    = models.FileField(null=True, blank=True)
    carousel = models.FileField(null=True, blank=True)
    findata = models.FileField(upload_to= 'csv/', null=True, blank=True)
    chose = (('office','Office'),('housing', 'Housing'),('hotel', 'Hotel'),('warehouse','Warehouse'),('retail', 'Retail'),('mixed-use','Mixed-use'))
    category = CharField(max_length=9, choices=chose, default='None')
    propname = models.CharField(max_length=30)
    street   = models.CharField(max_length=50)
    city     = models.CharField(max_length=50)
    state    = models.CharField(max_length=50)
    zipcode  = models.CharField(max_length=6)
    country  = models.CharField(max_length=50)
    descript = models.TextField(max_length=None)
    sdescript = models.TextField(max_length=300)
    updated  = models.DateTimeField(auto_now=True, auto_now_add=False)
    creation = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.propid

    def get_absolute_url(self):
        return reverse("client:proper", kwargs={'slug': self.slug})

class BackendVentureCapital(models.Model):
    ventid   = models.CharField(max_length=6)
    closed   = BooleanField(default=False)
    slug     = models.SlugField(unique=True)
    vname    = models.CharField(max_length=30)
    image    = models.FileField(null=True, blank=True)
    carousel = models.FileField(null=True, blank=True)
    findata = models.FileField(null=True, blank=True)
    descript = models.TextField(max_length=None)
    sdescript = models.TextField(max_length=300)
    updated  = models.DateTimeField(auto_now=True, auto_now_add=False)
    creation = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.ventid

    def get_absolute_url(self):
        return reverse("client:vc", kwargs={'slug': self.slug})

class Personal_Info(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    Image = models.FileField(upload_to= 'profile/img', null=True, blank=True)
    Cell_Phone = models.CharField(max_length=100)
    Home_Phone = models.CharField(max_length=100)
    Street     = models.CharField(max_length=100)
    City       = models.CharField(max_length=100)
    State      = models.CharField(max_length=100)
    Zipcode    = models.CharField(max_length=100)
    Country    = models.CharField(max_length=100)
