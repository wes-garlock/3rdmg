from django.conf.urls import url, include
from django.contrib import admin

from .views import (
    proper,
    properties,
    vcs,
    vc,
    profile,
    investments,
    update,

    )

urlpatterns = [
    url(r'^investments/$', investments, name='investments'),
    url(r'^profile/$', profile, name='profile'),
    url(r'^profile/update/$', update, name='update'),
    url(r'^realestate/$', properties, name='properties'),
    url(r'^realestate/(?P<slug>[\w-]+)/$', proper,name='proper'),
    url(r'^venturecapital/$',vcs,name='vcs'),
    url(r'^venturecapital/(?P<slug>[\w-]+)/$', vc,name='vc'),
]
