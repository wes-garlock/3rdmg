from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from .models import BackendBuilding, BackendVentureCapital, Personal_Info
from .methods import csvpull, watermarktool, csvapp
from .forms import UserForm, PersonalForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.backends import ModelBackend
from backend import HTML
import boto, boto3, sys, os, csv, json
from django.conf import settings


# Create your views here.

# Client View
@login_required
def profile(request):

    return render(request, 'profile.html', {})

@login_required
def investments(request):
    office = []
    hotel = []
    housing = []
    warehouse = []
    retail = []
    mixeduse = []
    proper = BackendBuilding.objects.all()
    for prop in proper:
        if request.user.has_perm('backend.'+prop.propid):
            if prop.category == 'office':
                office.append(prop)
            if prop.category == 'hotel':
                hotel.append(prop)
            if prop.category == 'housing':
                housing.append(prop)
            if prop.category == 'retail':
                retail.append(prop)
            if prop.category == 'warehouse':
                warehouse.append(prop)
            if prop.category == 'mixeduse':
                mixeduse.append(prop)

    context = {
        "prop"      : (office, hotel, housing, warehouse, retail, mixeduse),
    }
    return render(request, 'investments.html', context)

@login_required
def update(request):
    form1 = UserForm(request.POST or None, instance=request.user)
    form2 = PersonalForm(request.POST or None)
    try:
        form2 = PersonalForm(request.POST or None, instance=request.user.personal_info)
    except:
        pass

    if form1.is_valid():
        request.user = form1.save(commit=False)
        request.user.save()
        return HttpResponseRedirect('/client/profile')

    if form2.is_valid():
        request.user.personal_info = form2.save(commit=False)
        request.user.personal_info.save()
        return HttpResponseRedirect('/client/profile')

    context = {
        'form1' : form1,
        'form2' : form2,
    }
    return render(request, 'profileupdate.html', context)

# REAL ESTATE VIEWS

@login_required
def properties(request):
    office = BackendBuilding.objects.filter(category="office")
    hotel = BackendBuilding.objects.filter(category="hotel")
    housing = BackendBuilding.objects.filter(category="housing")
    warehouse = BackendBuilding.objects.filter(category="warehouse")
    retail = BackendBuilding.objects.filter(category="retail")
    mixeduse = BackendBuilding.objects.filter(category="mixed-use")

    context = {
        "prop"      : (office, hotel, housing, warehouse, retail, mixeduse),
    }

    return render(request, 'properties.html', context)

@login_required
def proper(request, slug=None):
    instance  = get_object_or_404(BackendBuilding, slug=slug)
    if request.user.has_perm('backend.'+instance.propid):
        if not instance.findata == None:
            context = csvapp(instance)
            context["prop"] = instance
            return render(request, 'property.html', context)
        else:
            context = {
                "prop" : instance,
            }
            return render(request, 'property.html', context)
    else:
        return render(request, 'profile.html', {})


# VENTURE CAPITAL VIEWS

@login_required
def vcs(request):
    queryset = BackendVentureCapital.objects.all()
    context = {
        "prop" : queryset,
    }
    return render(request, 'venturecapitals.html', context)

@login_required
def vc(request, slug=None):
    instance = get_object_or_404(BackendVentureCapital, slug=slug)
    if request.user.has_perm('backend.'+instance.ventid):
        if not instance.findata == None:
            context = csvapp(instance)
            context["prop"] = instance
            return render(request, 'venturecapital.html', context)
        else:
            context = {
                "prop" : instance,
            }
            return render(request, 'venturecapital.html', context)
    else:
        return render(request, 'profile.html', {})
