
#Created by Wesley Garlock Version 1.0 Published June 8th 2016
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from .models import Building, VentureCapital


# Register your models here.

class BuildingAdmin(admin.ModelAdmin):
    list_display = ["__unicode__",  "propname", "category", "country", "city", "state", "zipcode", "updated" ]
    list_display_links = ["__unicode__", "propname"]
    list_filter = ["country", "city", "state", "zipcode", "updated"]
    search_fields = ["__unicode__",  "propname", "country", "city", "state", "zipcode"]

    class Meta:
        model = Building

admin.site.register(Building, BuildingAdmin)

class VentureCapitalAdmin(admin.ModelAdmin):
    list_display = ["__unicode__", "vname", 'updated'  ]
    list_display_links = ["__unicode__",]
    list_filter = []
    search_fields = []

    class Meta:
        model = VentureCapital

admin.site.register(VentureCapital, VentureCapitalAdmin)
