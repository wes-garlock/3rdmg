from __future__ import unicode_literals
from django.conf import settings
from django.db import models
from django.db.models.fields import EmailField, IntegerField, CharField, BooleanField
from django.core.urlresolvers import reverse
from django.db.models.signals import pre_save
from django.contrib.auth.models import User



class Building(models.Model):
    propid   = models.CharField(max_length=6)
    closed   = BooleanField(default=False)
    slug     = models.SlugField(unique=True)
    image    = models.FileField(null=True, blank=True)
    carousel = models.FileField(null=True, blank=True)
    chose = (('office','Office'),('housing', 'Housing'),('hotel', 'Hotel'),('warehouse','Warehouse'),('retail', 'Retail'),('mixed-use','Mixed-use'))
    category = CharField(max_length=9, choices=chose, default='None')
    propname = models.CharField(max_length=30)
    street   = models.CharField(max_length=50)
    city     = models.CharField(max_length=50)
    state    = models.CharField(max_length=50)
    zipcode  = models.CharField(max_length=6)
    country  = models.CharField(max_length=50)
    descript = models.TextField(max_length=None)
    size     = models.CharField(max_length=50)
    built    = models.CharField(max_length=4)
    acquired = models.CharField(max_length=4)
    website  = models.CharField(max_length=50)
    updated  = models.DateTimeField(auto_now=True, auto_now_add=False)
    creation = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.propid

    def get_absolute_url(self):
        return reverse("client:proper", kwargs={'slug': self.slug})

class VentureCapital(models.Model):
    ventid   = models.CharField(max_length=6)
    closed   = BooleanField(default=False)
    slug     = models.SlugField(unique=True)
    vname    = models.CharField(max_length=30)
    image    = models.FileField(null=True, blank=True)
    carousel = models.FileField(null=True, blank=True)
    descript = models.TextField(max_length=None)
    sdescript = models.TextField(max_length=300)
    updated  = models.DateTimeField(auto_now=True, auto_now_add=False)
    creation = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __unicode__(self):
        return self.ventid

    def get_absolute_url(self):
        return reverse("client:vc", kwargs={'slug': self.slug})
