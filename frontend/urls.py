from django.conf.urls import url, include
from django.contrib import admin

from .views import (
    contact,
    home,
    properties,
    about,
    vcs
    )

urlpatterns = [
    url(r'^contact/$', contact, name='contact'),
    url(r'^properties/$', properties, name='properties'),
    url(r'^about/$', about, name='about'),
    url(r'^$', home, name='home'),
    url(r'^venturecapital/$', vcs, name='vcs'),
    
]
