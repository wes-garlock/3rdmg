from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from .models import Building, VentureCapital
from backend.models import BackendBuilding, BackendVentureCapital
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.backends import ModelBackend


# Create your views here.
def home(request):
    queryset = BackendBuilding.objects.all()
    context = {
        "prop" : queryset,

    }
    return render(request, 'home.html', context)

def contact(request):
    return render(request, 'contact.html', {})

def properties(request):
    boffice    = BackendBuilding.objects.filter(category="office")
    bhotel     = BackendBuilding.objects.filter(category="hotel")
    bhousing   = BackendBuilding.objects.filter(category="housing")
    bwarehouse = BackendBuilding.objects.filter(category="warehouse")
    bretail    = BackendBuilding.objects.filter(category="retail")
    bmixeduse  = BackendBuilding.objects.filter(category="mixed-use")
    office     = Building.objects.filter(category="office")
    hotel      = Building.objects.filter(category="hotel")
    housing    = Building.objects.filter(category="housing")
    warehouse  = Building.objects.filter(category="warehouse")
    retail     = Building.objects.filter(category="retail")
    mixeduse   = Building.objects.filter(category="mixed-use")

    context = {
        "prop"      : (office, hotel, housing, warehouse, retail, mixeduse),
        "backprop"  : (boffice, bhotel, bhousing, bwarehouse, bretail, bmixeduse),
    }
    return render(request, 'frontproperties.html', context)

def about(request):
    return render(request, 'about.html', {})

def vcs(request):
    queryset  = VentureCapital.objects.all()
    bqueryset = BackendVentureCapital.objects.all()
    context = {
        "prop" : queryset,
        "bprop" : bqueryset,
    }
    return render(request, 'frontventurecapitals.html', context)
