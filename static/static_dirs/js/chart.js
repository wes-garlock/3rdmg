google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
  var a = document.getElementById({{a|safe}}).value
  var data = google.visualization.arrayToDataTable(a);

  var options = {
    title: '',
    chartArea: {
      width: '75%'
    },
    pointSize: 10,
    series: {
          0: { pointShape: 'triangle' },
          1: { pointShape: 'square' },
          2: { pointShape: 'diamond' },
      },
    legend: {
      textStyle: {color: '#AFAFB9'},
      position: 'top',
    },
    hAxis: {
      viewWindowMode:'explicit',
      viewWindow: {
        max:"{{maxi|safe|floatformat:'0'}}",
        min:"{{mini|safe|floatformat:'0'}}"
      },
      title: 'Year',
      titleTextStyle: {color: '#657EC9'},
      textStyle: {color: '#AFAFB9'},
      baseline: "{{mini|safe|floatformat:'0'}}",
      baselineColor: '#657EC9',
      gridlines: {
        count:"{{length}}",
        color: '#ffffff',
      },
      format:'####'

    },
    vAxis: {
      minValue: 0,
      textStyle: {color: '#AFAFB9'},
      baselineColor: '#657EC9',
      format: 'short',
      title: 'US Dollars',
      titleTextStyle: {color: '#657EC9'},
      gridlines: {
        color: '#ffffff'
      }
    },
    tooltip: {
      isHtml: true,
      textStyle: {color: '#FEFFFE'},
      format: '####'
    },
    pointsVisible: true,

  };
  var formatter = new google.visualization.NumberFormat({pattern:'####'});
  formatter.format(data, 0);

  var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
  chart.draw(data, options);
}
